meta:
  id: motec_m48_emp
  file-extension: Q62
  endian: le
  encoding: ASCII
seq:
  - id: unknown1
    size: 2
  - id: ignition_type
    type: u4
    enum: ignition_type
  - id: version
    type: u2
  - id: fuel_used_cal
    type: u2
  - id: throttle_pos_closed
    type: u2
  - id: throttle_pos_open
    type: u2
  - id: unknown2
    size: 4
  - id: crank_index_position
    type: u2
enums:
  ignition_type:
    0x2001: motec_expander
  ignition_type_motec:
    1: motec_expander
    2: motec_expander_dual
  ignition_special_mode:
    1: motec_expander